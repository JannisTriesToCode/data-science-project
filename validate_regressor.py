import numpy as np
import pandas as pd
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import PolynomialFeatures

x_cats = ['OverallQual', 'GrLivArea', 'TotalBsmtSF',
          'YearBuilt', 'OverallCond', 'LotArea', 'GarageCars']
y_cat = 'SalePrice'


def validate_regressor(filename):
    data = pd.read_csv(filename, sep=';')
    regressor = train_regressor('SetFiltered.csv')
    validate_regressor_with_data(regressor, data)


def validate_regressor_with_data(regressor, data):
    x_true = data[x_cats]
    y_true = data[y_cat]
    y_pred = regressor.predict(x_true)
    print_regressor_metrics(y_true, y_pred)


def print_regressor_metrics(y_true, y_pred):
    metrics = {
        'R^2': 1 - np.sum((y_true - y_pred) ** 2) / np.sum((y_true - np.mean(y_true)) ** 2),
        'MSE': np.mean((y_true - y_pred) ** 2),
        'RMSE': np.sqrt(np.mean((y_true - y_pred) ** 2)),
        'MAPE': np.mean(np.abs(y_true - y_pred) / np.abs(y_true)),
        'MAX': np.max(np.abs(y_true - y_pred)),
    }
    max_metric_name_len = max(map(len, metrics.keys()))
    for metric, value in metrics.items():
        print(f'{metric:>{max_metric_name_len}}: {value:>22.10f}')


def train_regressor(filename):
    data = pd.read_csv(filename, sep=';')
    x_train = data[x_cats]
    y_train = data[y_cat]
    model = make_pipeline(PolynomialFeatures(
        2), GradientBoostingRegressor(random_state=43))
    model.fit(x_train, y_train)
    return model


print('Training data:')
validate_regressor('SetFiltered.csv')
print()
print('Validation data:')
validate_regressor('validate.csv')
