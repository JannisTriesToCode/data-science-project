# Sprachverarbeitung – Aktueller Stand und Entwicklungen 


Natural Language Processing (NLP), zu deutsch Sprachverbeitung, setzt rechnergestützte Techniken zum Lernen, Verstehen und Produzieren von Inhalten in menschlicher Sprache ein. [1] Diese Zusammenfassung beschätigt sich mit dem aktuellen Stand und den Entwicklungen.

---

Früher hauptsächlich: 
- maschinelle Übersetzung
- Spracherkennung 
- Sprachsynthese

Aktuelle Themen: 
- Systeme für den gesprochenen Dialog und Übersetzungsmaschinen von Sprache zu Sprache
- Mining von Socialmedia für Informationen über Gesundheit oder Finanzen 
- Identifizierung von Gefühlen und Emotionen im Zusammenhang mit Produkten und Dienstleistungen

Großes Wachstum (z.b. Siri) der letzten 20 Jahre ermöglicht durch vier Schlüsselfaktoren:
- großer Anstieg der Rechenleistung
- sehr große verfügbare Trainingssets
- Entwicklung von sehr erfolgreichen Algorithmen des Maschinellen Lernens (ML)
- besseres Verständnis der Struktur von menschlicher Sprache

Aktuell interessante Anwendungsgebiete basieren auf cutting-edge Methoden, welche statistische Analyse und ML mit Sprachkenntniss verbinden. Natural Language Processing (NLP), zu deutsch Sprachverbeitung, setzt rechnergestützte Techniken zum Lernen, Verstehen und Produzieren von Inhalten in menschlicher Sprache ein. Dabei können verschiedene Ziele verfolgt werden:
- die Mensch zu Mensch Kommunikation zu vereinfachen mit z.B. maschineller Übersetzung
    - State of the Art: neurale Netzte trainiert mit großen Datensätzen (12)
    - Kinderschuhe: eine Sequenz von Sätzen aufeinmal übersetzen (14)
    - Den Mensch mit einbeziehen, um Tipps von ihm zu erhalten (15)
- die Mensch zu Maschine Kommunikation mit bspw. Kommunikationsagenten
    - helfen (leich-) behinderten Personen (17)
    - Spoken Dialogue System (SDS) benötigt
        - Automatic Speech Recognition (ASR) &rarr; Was sagt der Mensch? 
        - Machine Translation (MT) & Dialogue Management (DM) &rarr; Was will der Mensch?
        - Text To Speech (TTS) 
        - braucht Errormanagement &rarr; Was machen wenn nicht verstanden?
    - Große Fortschritte wurden hier mit deep-learning Modellen, welche Audio Signale zu Abfolgen von Audio und Worte menschlicher Sprache mappen, erzielt (20)
    - Funktionieren in eingeschränktem Bereich sehr gut, aber nur wenn die Themen der Interaktion vorher schon bekannt sind (während Training)
    - Die Latenz von SDSs ist höher als bei Menschen in einer normalen Konversation, was den Fluss dieser stören kann. Es wird daran geforscht die Schritte ASR, MT und TTS inkrementell zu bearbeiten, schon während der Mensch noch spricht.
    - Menschen passen sich im Laufe des Gespräches an ihr Gegenüber an (Tonart, Sprache, Gestik, ...). Wie ein SDS das umsetzen kann wird aktuell auch erforscht (23). Dazu wird aktuell hautpsächlich der Partially Observable Markov Decision Process (POMDP) genutzt (25)
- Mensch und Maschine Vorteile verschaffen durch das Analysieren des riesigen online-verfügbaren Satzes an menschlicher Sprache
    - Machine Reading (MR) wird immer relevanter, da die Mengen an Wissen exponentiell steigt, sowohl die tatsächliche Menge als auch die online verfügbare. (27) Wissenschaftler sind nicht in der Lage aus eigener Kraft auf dem Laufenden zu belieben. MR soll unter anderem Texte zusammenfassen. Dazu wird ML verwendet.
    - Es gibt riesige strukturierte wissenschaftliche Datenbanken (32). Mit AI das Wissen aus diesen zu extrahieren und Schlussfolgerung mit Hypothesengenerierung zu ermöglichen ist ein großes Forschungsziel (34).
    - Soziale Medien bieten einen riesigen Datensatz an Konversationen, welcher leicht mit Webscrapern erfasst werden kan. Aus diesen können extrem viele Informationen wie Demographie einzelner Personen, aber auch Trends und vieles mehr extrahiert werden. Diese Informationen sind für Werbetreibende, Regierungen und viele weitere Player extrem wertvoll.
    - SDS kann es Menschen aus unterenwickelten Ländern ermöglichen leicht an Informationen zu kommen (über Sprachsteuerung, da möglicherw. analphabetisch)
    - Nachteil von soziale Medien:
        - Privatsphäre
        - Schwer ein "Ground Truth" zu finden

@Felix
Ausgelassen habe ich "Analysis and generation of speaker state", weil es noch sehr theoretisch ist. Aber in nem Satz sicherlich erwähenswert. "Conclusion and outlook" hab ich nicht zusammengefasst, da es schon eine Zusammenfassung ist.




---
## Quellen

- [1] Julia Hirschberg and Christopher D. Manning, Advances in naturallanguage processing  https://cs224d.stanford.edu/papers/advances.pdf