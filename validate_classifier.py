import numpy as np
import pandas as pd
from sklearn.metrics import confusion_matrix
from sklearn.neighbors import KNeighborsClassifier
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler

x_cats = ['OverallQual', 'GrLivArea', 'TotalBsmtSF',
          'YearBuilt', 'OverallCond', 'LotArea', 'GarageCars']
y_cat = 'CentralAir'


def validate_classifier(filename):
    data = pd.read_csv(filename, sep=';')
    classifier = train_classifier('SetFiltered.csv')
    validate_classifier_with_data(classifier, data)


def validate_classifier_with_data(classifier, data):
    x_true = data[x_cats]
    y_true = data[y_cat]
    y_pred = classifier.predict(x_true)
    print_classifier_metrics(y_true, y_pred)


def print_classifier_metrics(y_true, y_pred):
    tn, fp, fn, tp = confusion_matrix(y_true, y_pred).ravel()
    metrics = {
        'Accuracy': (tn + tp) / len(y_pred),
        'False positives': fp,
        'False negatives': fn,
    }
    max_metric_name_len = max(map(len, metrics.keys()))
    for metric, value in metrics.items():
        print(f'{metric:>{max_metric_name_len}}: {value:>22.10f}')


def train_classifier(filename):
    data = pd.read_csv(filename, sep=';')
    x_train = data[x_cats]
    y_train = data[y_cat]
    model = make_pipeline(StandardScaler(), KNeighborsClassifier(
        n_neighbors=5, weights='distance'))
    model.fit(x_train, y_train)
    return model


print('Training data:')
validate_classifier('SetFiltered.csv')
print()
print('Validation data:')
validate_classifier('validate.csv')
